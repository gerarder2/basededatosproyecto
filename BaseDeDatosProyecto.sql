GO
Create database asd
GO
USE [asd]
GO

/****** Object:  Table [dbo].[Productos]    Script Date: 15/12/2021 10:55:49 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Productos](
	[idProd] [int] IDENTITY(1,1) NOT NULL,
	[nomProd] [varchar](50) NOT NULL,
	[precProducto] [float] NOT NULL,
	[estatus] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[idProd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

GO

/****** Object:  Table [dbo].[Sucursales]    Script Date: 15/12/2021 10:56:18 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Sucursales](
	[idSuc] [int] IDENTITY(1,1) NOT NULL,
	[nomSuc] [varchar](50) NOT NULL,
	[domSuc] [varchar](50) NOT NULL,
	[estatus] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[idSuc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

GO

/****** Object:  Table [dbo].[Venta]    Script Date: 15/12/2021 10:56:35 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Venta](
	[folioVenta] [int] IDENTITY(1,1) NOT NULL,
	[sucursal_id] [int] NULL,
	[subtotal] [float] NULL,
	[iva] [float] NULL,
	[total] [float] NULL,
	[fechaVenta] [date] NULL,
 CONSTRAINT [PK__Venta__63751B9A182EC281] PRIMARY KEY CLUSTERED 
(
	[folioVenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Venta]  WITH CHECK ADD  CONSTRAINT [FK__Venta__sucursal___286302EC] FOREIGN KEY([sucursal_id])
REFERENCES [dbo].[Sucursales] ([idSuc])
GO

ALTER TABLE [dbo].[Venta] CHECK CONSTRAINT [FK__Venta__sucursal___286302EC]
GO

GO

/****** Object:  Table [dbo].[Detalle_Venta]    Script Date: 15/12/2021 10:56:46 p. m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Detalle_Venta](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[folioVenta_id] [int] NULL,
	[producto_id] [int] NULL,
	[precio] [float] NULL,
	[cantidad] [int] NOT NULL,
	[importe] [float] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Detalle_Venta]  WITH CHECK ADD  CONSTRAINT [FK__Detalle_V__folio__2A4B4B5E] FOREIGN KEY([folioVenta_id])
REFERENCES [dbo].[Venta] ([folioVenta])
GO

ALTER TABLE [dbo].[Detalle_Venta] CHECK CONSTRAINT [FK__Detalle_V__folio__2A4B4B5E]
GO

ALTER TABLE [dbo].[Detalle_Venta]  WITH CHECK ADD  CONSTRAINT [FK__Detalle_V__produ__2B3F6F97] FOREIGN KEY([producto_id])
REFERENCES [dbo].[Productos] ([idProd])
GO

ALTER TABLE [dbo].[Detalle_Venta] CHECK CONSTRAINT [FK__Detalle_V__produ__2B3F6F97]
GO



SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[agregarDetalleVenta]
(
	@idVenta int,
	@idProducto int,
	@precio float,
	@cantidad int,
	@importe float
)
AS
BEGIN
	
	SET NOCOUNT ON;
	INSERT INTO Detalle_Venta(folioVenta_id, producto_id, precio, cantidad, importe)
	VALUES (@idVenta, @idProducto, @precio, @cantidad, @importe)
END

GO
/****** Object:  StoredProcedure [dbo].[agregarProducto]    Script Date: 15/12/2021 10:44:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create procedure [dbo].[agregarProducto]
@nomProd varchar(50),
@precioProd int,
@estatus bit
as
begin
    insert into Productos (nomProd, precProducto, estatus)
    values (@nomProd, @precioProd, @estatus)
end

GO
/****** Object:  StoredProcedure [dbo].[agregarSuc]    Script Date: 15/12/2021 10:45:42 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create procedure [dbo].[agregarSuc]
@nomSuc varchar(50),
@domSuc varchar(50), 
@estatus bit
as
begin
    insert into Sucursales(nomSuc, domSuc, estatus)
    values (@nomSuc, @domSuc, @estatus)
end

GO
/****** Object:  StoredProcedure [dbo].[agregarVenta]    Script Date: 15/12/2021 10:46:16 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[agregarVenta]
(
	@sucId int,
	@subTotal float,
	@iva float,
	@total float
)
AS
BEGIN
	
	SET NOCOUNT ON;
	INSERT INTO Venta (sucursal_id, subtotal, iva, total, fechaVenta)
	VALUES (@sucId, @subTotal, @iva, @total, getDate())
END

GO
/****** Object:  StoredProcedure [dbo].[ConsultarDetallesVentaRangoFechas]    Script Date: 15/12/2021 10:46:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[ConsultarDetallesVentaRangoFechas]
(
    @sucursal_id int,
	@fechaInicial datetime,
	@fechaFinal datetime
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT
	DV.folioVenta_id, 
	DV.producto_id,
	P.nomProd,
	DV.cantidad,
	DV.importe
	
FROM 
	VENTA AS V 
	INNER JOIN DETALLE_VENTA AS DV ON V.sucursal_id = @sucursal_id
	INNER JOIN Productos AS P ON DV.producto_id = P.idProd 
WHERE 
	V.fechaVenta BETWEEN @fechaInicial AND @fechaFinal
	ORDER BY fechaVenta DESC;
END

GO
/****** Object:  StoredProcedure [dbo].[ConsultarDetalleVentaFolio]    Script Date: 15/12/2021 10:46:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[ConsultarDetalleVentaFolio]
   @folioVenta_id int

AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT 
	V.folioVenta,
	P.idProd, 
	nomProd, 
	cantidad,  
	importe
FROM 
	VENTA AS V 
	INNER JOIN DETALLE_VENTA AS DV ON V.folioVenta = DV.folioVenta_id
	INNER JOIN Productos AS P ON DV.producto_id = P.idProd 
WHERE 
	DV.folioVenta_id=@folioVenta_id
END

GO
/****** Object:  StoredProcedure [dbo].[ConsultarProducto]    Script Date: 15/12/2021 10:47:26 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[ConsultarProducto]
(
	@idProd int,
	@nomProd varchar(50)
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT * FROM Productos
	WHERE idProd=@idProd or nomProd=@nomProd
END

GO
/****** Object:  StoredProcedure [dbo].[ConsultarSucursal]    Script Date: 15/12/2021 10:47:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[ConsultarSucursal]
(
	@idSuc int,
	@nomSuc varchar(50)
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT * FROM Sucursales
	WHERE idSuc=@idSuc or nomSuc=@nomSuc
END

GO
/****** Object:  StoredProcedure [dbo].[ConsultarTodosProductos]    Script Date: 15/12/2021 10:48:11 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ConsultarTodosProductos]
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT * FROM Productos
END


GO
/****** Object:  StoredProcedure [dbo].[ConsultarTodosSucursales]    Script Date: 15/12/2021 10:48:36 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ConsultarTodosSucursales]
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT * FROM Sucursales
END

GO
/****** Object:  StoredProcedure [dbo].[ConsultarVenta]    Script Date: 15/12/2021 10:48:46 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[ConsultarVenta]
(
	@folioVenta int
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT * FROM Venta
	WHERE folioVenta=@folioVenta

END

GO
/****** Object:  StoredProcedure [dbo].[ConsultarVentaRangoFechas]    Script Date: 15/12/2021 10:49:27 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[ConsultarVentaRangoFechas]
(
    @sucursal_id int,
	@fechaInicial datetime,
	@fechaFinal datetime
)
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT 
	V.folioVenta,
	V.sucursal_id,
	V.subtotal,
	V.iva,
	V.total,
	V.fechaVenta
	
FROM 
	VENTA AS V 
WHERE 
	sucursal_id=@sucursal_id AND V.fechaVenta BETWEEN @fechaInicial AND @fechaFinal
	ORDER BY fechaVenta DESC;
END

GO
/****** Object:  StoredProcedure [dbo].[EliminarProducto]    Script Date: 15/12/2021 10:49:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[EliminarProducto]
(
	@idProducto int
)
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM Productos
	WHERE idProd = @idProducto
END

GO
/****** Object:  StoredProcedure [dbo].[EliminarSucursal]    Script Date: 15/12/2021 10:50:17 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[EliminarSucursal]
@idSuc int
AS
BEGIN
	
	SET NOCOUNT ON;

    DELETE FROM Sucursales
	WHERE idSuc = @idSuc
END

GO
/****** Object:  StoredProcedure [dbo].[EliminarVenta]    Script Date: 15/12/2021 10:50:27 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[EliminarVenta]
	@id int
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM Detalle_Venta WHERE folioVenta_id = @id
	DELETE FROM Venta WHERE folioVenta =  @id
END

GO
/****** Object:  StoredProcedure [dbo].[ModificarProducto]    Script Date: 15/12/2021 10:50:38 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ModificarProducto]
(
@idProd int,
@nomProd varchar(50),
@precProducto float,
@estatus bit
)
AS
BEGIN
	
	SET NOCOUNT ON;
	UPDATE Productos SET nomProd = @nomProd, precProducto = @precProducto, estatus=@estatus
	WHERE idProd = @idProd
END

GO
/****** Object:  StoredProcedure [dbo].[ModificarSucursal]    Script Date: 15/12/2021 10:50:46 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ModificarSucursal]
(
@idSuc int,
@nomSuc varchar(50),
@domSuc varchar(50),
@estatus bit
)
AS
BEGIN
	
	SET NOCOUNT ON;
	UPDATE Sucursales SET nomSuc = @nomSuc, domSuc = @domSuc, estatus=@estatus
	WHERE idSuc = @idSuc
END