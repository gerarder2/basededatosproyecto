
package proyectobd;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

public class ConsultarVenta extends javax.swing.JFrame {
    static Connection con = null;
    String folioVenta;
    String sucursal_id;
    String fechaInicial;
    String fechaFinal;

    
    public ConsultarVenta() {
        initComponents();
        con = getConnection();
    }
    
    public static Connection getConnection(){
        
        String connectionURL = "jdbc:sqlserver://localhost:1433;"
                +"database=PuntoDeVenta;"
                +"username=tarea;"
                +"password=1234;";
        
        try {
            Connection con = DriverManager.getConnection(connectionURL);
            System.out.println("Conectado");
            return con;
        }catch(SQLException ex){
            System.out.println(ex);
            return null;
        }
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtfolioVenta = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtFechaInicial = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtFechaFinal = new javax.swing.JTextField();
        btnBuscarFolio = new javax.swing.JButton();
        btnBuscarporFechas = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaVenta = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaDetalle_Venta = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtSucursalid = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Reportes Venta");

        jLabel2.setText("folioVenta:");

        jLabel3.setText("Fecha Inicial:");

        jLabel4.setText("Fecha Final:");

        btnBuscarFolio.setText("Buscar");
        btnBuscarFolio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarFolioActionPerformed(evt);
            }
        });

        btnBuscarporFechas.setText("Buscar");
        btnBuscarporFechas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarporFechasActionPerformed(evt);
            }
        });

        tablaVenta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID de venta", "ID de sucursal", "Sub Total", "IVA", "Total", "Fecha"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaVenta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaVentaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaVenta);

        tablaDetalle_Venta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "folioVenta", "idProd", "nomProd", "cantidad", "importe"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tablaDetalle_Venta);

        jLabel5.setText("Venta");

        jLabel6.setText("Detalle_Venta");

        jLabel7.setText("ID sucursal");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addGap(18, 18, 18)
                                        .addComponent(txtfolioVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(jLabel3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtFechaInicial))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel7)
                                        .addGap(18, 18, 18)
                                        .addComponent(txtSucursalid, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnBuscarFolio)
                                    .addComponent(txtFechaFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btnBuscarporFechas)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 634, Short.MAX_VALUE)
                            .addComponent(jScrollPane2))))
                .addContainerGap(75, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtfolioVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnBuscarFolio)))
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtFechaInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(txtFechaFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtSucursalid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7))
                        .addGap(18, 26, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnBuscarporFechas)
                        .addGap(13, 13, 13)))
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarFolioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarFolioActionPerformed
        DefaultTableModel modelo=(DefaultTableModel) tablaVenta.getModel();
        modelo.getDataVector().removeAllElements();
        
        Object [] fila=new Object[6];
        try{
            CallableStatement call = con.prepareCall("{call ConsultarVenta(?)}");
            
            call.setString(1, txtfolioVenta.getText());
            ResultSet res = call.executeQuery();
            
            while(res.next()) {
                fila[0]=res.getString(1);
                fila[1]=res.getString(2);
                fila[2]=res.getString(3);
                fila[3]=res.getString(4);
                fila[4]=res.getString(5);
                fila[5]=res.getString(6);
                modelo.addRow(fila);
            }
            LlenarDetalle(txtfolioVenta.getText());
        }catch(SQLException ex){
            Logger.getLogger(ConsultarSucursal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnBuscarFolioActionPerformed

    private void btnBuscarporFechasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarporFechasActionPerformed
       DefaultTableModel modelo=(DefaultTableModel) tablaVenta.getModel();
        modelo.getDataVector().removeAllElements();
        
        Object [] fila=new Object[6];
        try{
            CallableStatement call = con.prepareCall("{call ConsultarVentaRangoFechas(?,?,?)}");
            
            call.setString(1, txtSucursalid.getText());
            call.setString(2, txtFechaInicial.getText());
            call.setString(3, txtFechaFinal.getText());
            ResultSet res = call.executeQuery();
            
            while(res.next()) {
                fila[0]=res.getString(1);
                fila[1]=res.getString(2);
                fila[2]=res.getString(3);
                fila[3]=res.getString(4);
                fila[4]=res.getString(5);
                fila[5]=res.getString(6);
                modelo.addRow(fila);
            }
            LlenarDetalle2();
        }catch(SQLException ex){
            Logger.getLogger(ConsultarSucursal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnBuscarporFechasActionPerformed

    private void tablaVentaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaVentaMouseClicked
        
    }//GEN-LAST:event_tablaVentaMouseClicked

   
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ConsultarVenta().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarFolio;
    private javax.swing.JButton btnBuscarporFechas;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tablaDetalle_Venta;
    private javax.swing.JTable tablaVenta;
    private javax.swing.JTextField txtFechaFinal;
    private javax.swing.JTextField txtFechaInicial;
    private javax.swing.JTextField txtSucursalid;
    private javax.swing.JTextField txtfolioVenta;
    // End of variables declaration//GEN-END:variables

    private void LlenarDetalle(String folio) {
        DefaultTableModel modelo=(DefaultTableModel) tablaDetalle_Venta.getModel();
        modelo.getDataVector().removeAllElements();
        
        Object [] fila=new Object[5];
        try{
            CallableStatement call = con.prepareCall("{call ConsultarDetalleVentaFolio(?)}");
            
            call.setString(1, folio);
            ResultSet res = call.executeQuery();
            
            while(res.next()) {
                fila[0]=res.getString(1);
                fila[1]=res.getString(2);
                fila[2]=res.getString(3);
                fila[3]=res.getString(4);
                fila[4]=res.getString(5);
                modelo.addRow(fila);
            }
        }catch(SQLException ex){
            Logger.getLogger(ConsultarSucursal.class.getName()).log(Level.SEVERE, null, ex);
        }                               
    }
    private void LlenarDetalle2() {
        DefaultTableModel modelo=(DefaultTableModel) tablaDetalle_Venta.getModel();
        modelo.getDataVector().removeAllElements();
        
        Object [] fila=new Object[5];
        try{
            CallableStatement call = con.prepareCall("{call ConsultarDetallesVentaRangoFechas(?,?,?)}");
            
            call.setString(1, txtSucursalid.getText());
            call.setString(2, txtFechaInicial.getText());
            call.setString(3, txtFechaFinal.getText());
            ResultSet res = call.executeQuery();
            
            while(res.next()) {
                fila[0]=res.getString(1);
                fila[1]=res.getString(2);
                fila[2]=res.getString(3);
                fila[3]=res.getString(4);
                fila[4]=res.getString(5);
                modelo.addRow(fila);
            }
        }catch(SQLException ex){
            Logger.getLogger(ConsultarSucursal.class.getName()).log(Level.SEVERE, null, ex);
        }                               
    }
}
