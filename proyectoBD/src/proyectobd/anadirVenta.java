package proyectobd;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingConstants;

public class anadirVenta {
	
	public static JFrame frame;
	private JTable tablaProductos;
	private JTextField textFieldBuscar;
	private JTable tablaVenta;
	private JTextField textFieldSubtotal;
	private JTextField textFieldFolio;
	private JTextField textFieldIVA;
	private JTextField textFieldTotal;
	private JTextField textFieldPago;
	private JTextField textFieldCambio;
	private double totalConIVA;
	private double subtotal;
	public static JTextField textFieldSucursal;
	private JTextField textFieldFecha;
	
	// M�TODO PARA MOSTRAR TODOS LOS PRODUCTOS
	public ArrayList<Productos> listaProductos(){
		ArrayList<Productos> listaProducto = new ArrayList<>(); 
		Connection conexion = null;
		try {
			conexion = conectar.getConnection();
			conexion.setAutoCommit(false);
			String query="select * from Productos";
			Statement cst = conexion.createStatement();
			ResultSet rs = cst.executeQuery(query);
			Productos producto;
			while(rs.next()) {
				producto = new Productos(rs.getInt("idProd"), rs.getString("nomProd"), rs.getInt("precProducto"), rs.getInt("estatus"));
				listaProducto.add(producto);
			}
			
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(null, e1);
		}
		return listaProducto;
	}
	
	public void mostrarProductos() {
		ArrayList<Productos> list = listaProductos();
		DefaultTableModel model = (DefaultTableModel)tablaProductos.getModel();
		Object[] fila = new Object[4];
		for(int i=0;i<list.size();i++) {
			fila[0]=list.get(i).getIdProd();
			fila[1]=list.get(i).getNombre();
			fila[2]=list.get(i).getPrecio();
			fila[3]=list.get(i).getEstatus();
			model.addRow(fila);
		}
	}
	//TERMINA M�TODO MOSTRAR TODOS LOS PRODUCTOS
	
	// M�TODO PARA MOSTRAR PRODUCTOS BUSCADOS
	public ResultSet buscarProd(String nomProd) {
		Connection conexion = null;
		PreparedStatement pst;
		ResultSet rs = null;
		
		try {
			conexion = conectar.getConnection();
			conexion.setAutoCommit(false);
			
			pst = conexion.prepareStatement("select * from Productos where nomProd=?");
			pst.setString(1, nomProd);
			rs = pst.executeQuery();
			
		} catch (Exception e1) {
			
		} return rs;
	}
	
	private void buscarProducto(java.awt.event.ActionEvent evt) {
		String buscar = this.textFieldBuscar.getText();
		ResultSet rs = buscarProd(buscar);
		DefaultTableModel dfmBuscar = new DefaultTableModel();
		this.tablaProductos.setModel(dfmBuscar);
		dfmBuscar.setColumnIdentifiers(new Object[] {"ID", "Nombre","Precio","Estatus"});
		try {
			while(rs.next()) {
				dfmBuscar.addRow(new Object[] {rs.getInt("idProd"), rs.getString("nomProd"), rs.getDouble("precProducto"), rs.getInt("estatus")});
			}
		}catch (Exception e) {
			
		}
	}
	//TERMINA M�TODO PARA MOSTRAR ELEMENTOS BUSCADOS
	
	//INICIA M�TODO PARA A�ADIR PRODUCTO A LA COMPRA
	private void anadir(java.awt.event.ActionEvent evt) {
		
		String cantidad = JOptionPane.showInputDialog("Ingrese la cantidad:");
		TableModel model1 = tablaProductos.getModel();
		int[] indexs = tablaProductos.getSelectedRows();
		Object[] fila = new Object[5];
		DefaultTableModel model = (DefaultTableModel)tablaVenta.getModel();
		
		for(int i=0;i<indexs.length;i++) {
			double precio = (double) ((Integer)model1.getValueAt(indexs[i], 2));
			double importe = precio * Double.parseDouble(cantidad);
			double aux;
			
			fila[0]=model1.getValueAt(indexs[i], 0);
			fila[1]=model1.getValueAt(indexs[i], 1);
			fila[2]=model1.getValueAt(indexs[i], 2);
			fila[3]=cantidad;
			fila[4]=importe;

			model.addRow(fila);
			aux = importe; 
			subtotal = subtotal + aux;
			double IVA = subtotal * .16;
			totalConIVA = subtotal + IVA;
			
			DecimalFormat df = new DecimalFormat("#.##");
			double dosDecimales = Double.valueOf(df.format(IVA));
			textFieldSubtotal.setText(Double.toString(subtotal));
			textFieldIVA.setText(Double.toString(dosDecimales));
			textFieldTotal.setText(Double.toString(totalConIVA));
		}		
	}
	//TERMINA M�TODO PARA A�ADIR PRODUCTO A LA COMPRA
	
	
	
	// INICIA M�TODO PARA QUITAR PRODUCTO DE LA COMPRA
	
	public void quitarProducto() {
		
		DefaultTableModel model = (DefaultTableModel)tablaVenta.getModel();
		int quitar = tablaVenta.getSelectedRow();
		model.removeRow(quitar);
	}
	
	//TERMINA M�TODO PARA QUITAR PRODUCTO DE LA COMPRA
	
	
	
	// INICIA M�TODO PARA CAMBIAR LA SUCURSAL
	public void cambiar() {
		this.frame.setVisible(false);
		cambiarSuc windows = new cambiarSuc();
		windows.frame.setVisible(true);
	}
	// TERMINA M�TODO PARA CAMBIAR LA SUCURSAL
	
	
	// INICIA M�TODO PARA MOSTRAR FECHA
	
	public static String fechaActual() {
		
		Date fecha = new Date();
		SimpleDateFormat formatoFecha=new SimpleDateFormat("dd/MM/YYYY");
		
		return formatoFecha.format(fecha);
		
	}
	 // TERMINA M�TODO PARA MOSTRAR FECHA
	
	
	// INICIA M�TODO PARA ALMACENAR VENTA EN LA BD
	
	public void anadirVentaBD() throws SQLException {
		Connection conexion = null;
	
		try {
			conexion = conectar.getConnection();
			conexion.setAutoCommit(false);

			
			CallableStatement anadirVentaPA = conexion.prepareCall("{call agregarVenta(?,?,?,?)}");
			anadirVentaPA.setString(1, textFieldSucursal.getText());
			anadirVentaPA.setString(2, textFieldSubtotal.getText());
			anadirVentaPA.setString(3, textFieldIVA.getText());
			anadirVentaPA.setString(4, textFieldTotal.getText());
			
			anadirVentaPA.execute();
			conexion.commit();
			
			
		} catch (Exception e) {
			conexion.rollback();
			e.printStackTrace();
		} finally {
			conexion.close();
		}
	}
	
	// TERMINA M�TODO PARA ALMACENAR VENTA EN LA BD
	
	
	// INICIA M�TODO PARA ALMACENAR DETALLE DE VENTA EN LA BD
	
		public void anadirDetalleVentaBD() throws SQLException {
			Connection conexion = null;
			Object folioVenta = 0;
		
			try {
				conexion = conectar.getConnection();
				conexion.setAutoCommit(false);
				
				
				//SELECT MAX(folioVenta) FROM Venta
				
				PreparedStatement pst = conexion.prepareStatement("SELECT MAX(folioVenta) FROM Venta");
				ResultSet rs = pst.executeQuery();
				while(rs.next()) {
					folioVenta = rs.getObject(1);
				}
				rs.close();
				pst.close();
				
				//textFieldCambio.setText(folioVenta);
				
				DefaultTableModel model = (DefaultTableModel) tablaVenta.getModel();
				
					 for(int i=0;i<model.getRowCount();i++) {
						
						Object idProd = model.getValueAt(i, 0);
						Object precProd = model.getValueAt(i, 2);
						Object cantidad = model.getValueAt(i, 3);
						Object importe = model.getValueAt(i, 4);
						
						CallableStatement anadirDetVentaPA = conexion.prepareCall("{call agregarDetalleVenta(?,?,?,?,?)}");
						anadirDetVentaPA.setObject(1, folioVenta);
						anadirDetVentaPA.setObject(2, idProd);
						anadirDetVentaPA.setObject(3, precProd);
						anadirDetVentaPA.setObject(4, cantidad);
						anadirDetVentaPA.setObject(5, importe);
						
						anadirDetVentaPA.execute();
						conexion.commit();
					}
					
			} catch (Exception e) {
				conexion.rollback();
				e.printStackTrace();
			} finally {
				conexion.close();
			}
		}
		
		// TERMINA M�TODO PARA ALMACENAR DETALLE DE VENTA EN LA BD

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					anadirVenta window = new anadirVenta();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public anadirVenta() {
		initialize();
		mostrarProductos();
		textFieldFecha.setText(fechaActual());
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings("serial")
	private void initialize() {
		
		// VENTANA
		
		frame = new JFrame();
		frame.setBounds(100, 100, 570, 520);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		// TABLAS
		
		JScrollPane scrollPaneLista = new JScrollPane();
		scrollPaneLista.setBounds(10, 44, 524, 94);
		frame.getContentPane().add(scrollPaneLista);
		
		tablaProductos = new JTable();
		tablaProductos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPaneLista.setViewportView(tablaProductos);
		tablaProductos.setBorder(new LineBorder(new Color(0, 0, 0)));
		tablaProductos.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ID", "Nombre", "Precio", "Estatus"
			}
		) {
			Class[] columnTypes = new Class[] {
				Object.class, Object.class, Object.class, int.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		tablaProductos.getColumnModel().getColumn(0).setResizable(false);
		tablaProductos.getColumnModel().getColumn(1).setResizable(false);
		tablaProductos.getColumnModel().getColumn(2).setResizable(false);
		
		
		JScrollPane scrollPaneVenta = new JScrollPane();
		scrollPaneVenta.setBounds(10, 224, 524, 71);
		frame.getContentPane().add(scrollPaneVenta);
		
		tablaVenta = new JTable();
		tablaVenta.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ID", "Nombre", "Precio Unitario", "Cantidad", "Importe"
			}
		) {
			boolean[] columnEditables = new boolean[] {
				false, false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		tablaVenta.getColumnModel().getColumn(0).setResizable(false);
		tablaVenta.getColumnModel().getColumn(1).setResizable(false);
		tablaVenta.getColumnModel().getColumn(2).setResizable(false);
		tablaVenta.getColumnModel().getColumn(2).setPreferredWidth(85);
		tablaVenta.getColumnModel().getColumn(3).setResizable(false);
		tablaVenta.getColumnModel().getColumn(4).setResizable(false);
		scrollPaneVenta.setViewportView(tablaVenta);
		
		
		
		// CUADROS DE TEXTO
		
		textFieldBuscar = new JTextField();
		textFieldBuscar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				textFieldBuscar.setText("");
			}
		});
		textFieldBuscar.setText("Ingrese B\u00FAsqueda");
		textFieldBuscar.setToolTipText("");
		textFieldBuscar.setBounds(302, 11, 132, 22);
		frame.getContentPane().add(textFieldBuscar);
		textFieldBuscar.setColumns(10);
		
		textFieldSubtotal = new JTextField();
		textFieldSubtotal.setBackground(Color.WHITE);
		textFieldSubtotal.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldSubtotal.setFont(new Font("Tahoma", Font.PLAIN, 11));
		textFieldSubtotal.setEditable(false);
		
		textFieldSubtotal.setBounds(422, 303, 86, 20);
		frame.getContentPane().add(textFieldSubtotal);
		textFieldSubtotal.setColumns(10);
		
		textFieldFolio = new JTextField();
		textFieldFolio.setEditable(false);
		textFieldFolio.setBounds(76, 196, 86, 20);
		frame.getContentPane().add(textFieldFolio);
		textFieldFolio.setColumns(10);
		
		textFieldIVA = new JTextField();
		textFieldIVA.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldIVA.setFont(new Font("Tahoma", Font.PLAIN, 11));
		textFieldIVA.setEditable(false);
		textFieldIVA.setColumns(10);
		textFieldIVA.setBackground(Color.WHITE);
		textFieldIVA.setBounds(422, 334, 86, 20);
		frame.getContentPane().add(textFieldIVA);
		
		textFieldTotal = new JTextField();
		textFieldTotal.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldTotal.setFont(new Font("Tahoma", Font.BOLD, 15));
		textFieldTotal.setEditable(false);
		textFieldTotal.setColumns(10);
		textFieldTotal.setBackground(Color.WHITE);
		textFieldTotal.setBounds(422, 366, 86, 20);
		frame.getContentPane().add(textFieldTotal);
		
		textFieldPago = new JTextField();
		textFieldPago.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DecimalFormat df = new DecimalFormat("#.##");
				double pago = Double.parseDouble(textFieldPago.getText());
				double cambio = pago - totalConIVA;

				double dosDecimales = Double.valueOf(df.format(cambio));
				
				textFieldCambio.setText(Double.toString(dosDecimales));
			}
		});
		textFieldPago.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldPago.setFont(new Font("Tahoma", Font.PLAIN, 11));
		textFieldPago.setColumns(10);
		textFieldPago.setBackground(Color.WHITE);
		textFieldPago.setBounds(422, 397, 86, 20);
		frame.getContentPane().add(textFieldPago);
		
		textFieldCambio = new JTextField();
		textFieldCambio.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldCambio.setFont(new Font("Tahoma", Font.PLAIN, 11));
		textFieldCambio.setEditable(false);
		textFieldCambio.setColumns(10);
		textFieldCambio.setBackground(Color.WHITE);
		textFieldCambio.setBounds(422, 425, 86, 20);
		frame.getContentPane().add(textFieldCambio);
		
		textFieldSucursal = new JTextField();
		textFieldSucursal.setEditable(false);
		textFieldSucursal.setColumns(10);
		textFieldSucursal.setBounds(234, 196, 63, 20);
		frame.getContentPane().add(textFieldSucursal);
		
		textFieldFecha = new JTextField();
		textFieldFecha.setEditable(false);
		textFieldFecha.setColumns(10);
		textFieldFecha.setBounds(448, 196, 86, 20);
		frame.getContentPane().add(textFieldFecha);
		
		
		
		// ETIQUETAS
		
		JLabel lblIVA = new JLabel("16% IVA");
		lblIVA.setBounds(349, 337, 63, 14);
		frame.getContentPane().add(lblIVA);
		
		JLabel lblTitulo = new JLabel("A\u00F1adir venta");
		lblTitulo.setFont(new Font("Dialog", Font.PLAIN, 24));
		lblTitulo.setBounds(10, 11, 179, 22);
		frame.getContentPane().add(lblTitulo);
		
		JLabel lblSubtotal = new JLabel("Subtotal:");
		lblSubtotal.setBounds(349, 306, 63, 14);
		frame.getContentPane().add(lblSubtotal);
		
		JLabel lblFolio = new JLabel("Folio venta:");
		lblFolio.setBounds(10, 199, 89, 14);
		frame.getContentPane().add(lblFolio);
		
		JLabel lblTotal = new JLabel("TOTAL:");
		lblTotal.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTotal.setBounds(349, 371, 63, 14);
		frame.getContentPane().add(lblTotal);
		
		JLabel lblPagaCon = new JLabel("Paga con:");
		lblPagaCon.setBounds(349, 400, 63, 14);
		frame.getContentPane().add(lblPagaCon);
		
		JLabel lblCambio = new JLabel("Cambio:");
		lblCambio.setBounds(349, 428, 63, 14);
		frame.getContentPane().add(lblCambio);
		
		JLabel lblSucursal = new JLabel("Sucursal:");
		lblSucursal.setBounds(179, 199, 77, 14);
		frame.getContentPane().add(lblSucursal);
		
		JLabel lblFecha = new JLabel("Fecha:");
		lblFecha.setBounds(405, 199, 46, 14);
		frame.getContentPane().add(lblFecha);
		
		
		// BOTONES
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscarProducto(e);
			}
		});
		btnBuscar.setBounds(445, 11, 89, 23);
		frame.getContentPane().add(btnBuscar);
		
		JButton btnAnadir = new JButton("A\u00F1adir");
		btnAnadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				anadir(e);
			}
		});
		btnAnadir.setBounds(445, 149, 89, 23);
		frame.getContentPane().add(btnAnadir);
		
		JButton btnQuitar = new JButton("Quitar");
		btnQuitar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				quitarProducto();
			}
		});
		btnQuitar.setBounds(10, 306, 89, 23);
		frame.getContentPane().add(btnQuitar);
		
		JButton btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					anadirVentaBD();
					anadirDetalleVentaBD();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnAceptar.setBounds(10, 424, 89, 23);
		frame.getContentPane().add(btnAceptar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		btnCancelar.setBounds(109, 424, 89, 23);
		frame.getContentPane().add(btnCancelar);
		
		
		JButton btnCambiarSuc = new JButton("Cambiar");
		btnCambiarSuc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cambiar();
			}
		});
		btnCambiarSuc.setBounds(307, 195, 77, 23);
		frame.getContentPane().add(btnCambiarSuc);
		
		
		
	}
}
