package proyectobd;

import java.awt.Container;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class anadirSucursal {

	JFrame frame;
	private JTextField textFieldNombre;
	private JTextField textFieldDir;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					anadirSucursal window = new anadirSucursal();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public anadirSucursal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel JLabel11 = new JLabel("A\u00F1adir producto");
		JLabel11.setFont(new Font("Dialog", Font.PLAIN, 24));
		JLabel11.setBounds(10, 11, 286, 22);
		frame.getContentPane().add(JLabel11);
		
		JLabel nombreLabel = new JLabel("Nombre:");
		nombreLabel.setBounds(34, 59, 70, 14);
		frame.getContentPane().add(nombreLabel);
		
		JLabel dirLabel = new JLabel("Direcci�n:");
		dirLabel.setBounds(34, 103, 70, 14);
		frame.getContentPane().add(dirLabel);
		
		textFieldNombre = new JTextField();
		textFieldNombre.setBounds(105, 56, 86, 20);
		frame.getContentPane().add(textFieldNombre);
		textFieldNombre.setColumns(10);
		
		textFieldDir = new JTextField();
		textFieldDir.setColumns(10);
		textFieldDir.setBounds(105, 100, 86, 20);
		frame.getContentPane().add(textFieldDir);
		
		JButton btnAnadir = new JButton("A\u00F1adir");
		btnAnadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					anadirSuc();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnAnadir.setBounds(105, 158, 89, 23);
		frame.getContentPane().add(btnAnadir);
		
		
		
	}
	
	public void anadirSuc() throws SQLException {
		Connection conexion = null;
		
		int estatus = 1;
		
		
		try {
			conexion = conectar.getConnection();
			conexion.setAutoCommit(false);
			
			CallableStatement anadirSucPA = conexion.prepareCall("{call agregarSuc(?,?,?)}");
			anadirSucPA.setString(1, textFieldNombre.getText());
			anadirSucPA.setString(2, textFieldDir.getText());
			anadirSucPA.setInt(3, estatus);
			
			anadirSucPA.execute();
			conexion.commit();
			
			
			
		} catch (Exception e) {
			conexion.rollback();
			e.printStackTrace();
		} finally {
			conexion.close();
		}
	}
}
