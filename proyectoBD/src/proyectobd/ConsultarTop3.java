package proyectobd;

import java.awt.EventQueue;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import java.awt.Font;

public class ConsultarTop3 {

	public JFrame frmTopProductos;
	private JTable tablaTop3;
	
	
	public void LlenarTabla(){
		Connection conexion = null;
		
       DefaultTableModel modelo=(DefaultTableModel) tablaTop3.getModel();
       modelo.getDataVector().removeAllElements(); 
       Object [] fila=new Object[4];
       try {
    	   conexion = conectar.getConnection();
    	   conexion.setAutoCommit(false);

           CallableStatement call = conexion.prepareCall("SELECT TOP 3\r\n"
           		+ "p.idProd,\r\n"
           		+ "p.nomProd,\r\n"
           		+ "p.precProducto as unitBasePrice,\r\n"
           		+ "sum(DV.cantidad) as quantitySold,\r\n"
           		+ "sum(DV.precio*DV.cantidad) as totalAmount\r\n"
           		+ "from\r\n"
           		+ "Venta V\r\n"
           		+ "inner join Detalle_Venta DV on DV.folioVenta_id=V.folioVenta\r\n"
           		+ "inner join Productos P on P.idProd=DV.producto_id\r\n"
           		+ "group by\r\n"
           		+ "p.idProd,p.nomProd,p.precProducto\r\n"
           		+ "order by quantitySold desc");
           
           ResultSet res = call.executeQuery();
           
           while (res.next()){
               fila[0]=res.getString(1);
               fila[1]=res.getString(2);
               fila[2]=res.getString(4);

               modelo.addRow(fila); 
           }
       } catch (SQLException ex) {
           Logger.getLogger(ConsultarProducto.class.getName()).log(Level.SEVERE, null, ex);
       }
   }

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConsultarTop3 window = new ConsultarTop3();
					window.frmTopProductos.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ConsultarTop3() {
		initialize();
		LlenarTabla();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTopProductos = new JFrame();
		frmTopProductos.setFont(new Font("Dialog", Font.PLAIN, 14));
		frmTopProductos.setTitle("Top 3 productos m\u00E1s vendidos");
		frmTopProductos.setBounds(100, 100, 450, 166);
		frmTopProductos.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmTopProductos.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(42, 26, 349, 80);
		frmTopProductos.getContentPane().add(scrollPane);
		
		tablaTop3 = new JTable();
		tablaTop3.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ID", "Nombre", "Cantidad Vendida"
			}
		));
		tablaTop3.getColumnModel().getColumn(2).setPreferredWidth(99);
		scrollPane.setViewportView(tablaTop3);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setFont(new Font("Dialog", Font.PLAIN, 24));
		lblNewLabel.setBounds(10, 11, 414, 55);
		frmTopProductos.getContentPane().add(lblNewLabel);
	}

}
