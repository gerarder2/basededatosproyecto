package proyectobd;

import java.awt.Container;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class anadirProducto {

	JFrame frame;
	private JTextField textFieldNombre;
	private JTextField textFieldPrecio;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					anadirProducto window = new anadirProducto();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public anadirProducto() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel JLabel11 = new JLabel("A\u00F1adir producto");
		JLabel11.setFont(new Font("Dialog", Font.PLAIN, 24));
		JLabel11.setBounds(10, 11, 286, 22);
		frame.getContentPane().add(JLabel11);
		
		JLabel nombreLabel = new JLabel("Nombre:");
		nombreLabel.setBounds(34, 59, 70, 14);
		frame.getContentPane().add(nombreLabel);
		
		JLabel precioLabel = new JLabel("Precio:");
		precioLabel.setBounds(34, 103, 70, 14);
		frame.getContentPane().add(precioLabel);
		
		textFieldNombre = new JTextField();
		textFieldNombre.setBounds(105, 56, 86, 20);
		frame.getContentPane().add(textFieldNombre);
		textFieldNombre.setColumns(10);
		
		textFieldPrecio = new JTextField();
		textFieldPrecio.setColumns(10);
		textFieldPrecio.setBounds(105, 100, 86, 20);
		frame.getContentPane().add(textFieldPrecio);
		
		JButton btnAnadir = new JButton("A\u00F1adir");
		btnAnadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					anadirProd();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnAnadir.setBounds(105, 158, 89, 23);
		frame.getContentPane().add(btnAnadir);
		
		
		
	}
	
	public void anadirProd() throws SQLException {
		Connection conexion = null;
		
		int estatus = 1;
		
		
		try {
			conexion = conectar.getConnection();
			conexion.setAutoCommit(false);
			
			CallableStatement anadirProdPA = conexion.prepareCall("{call agregarProducto(?,?,?)}");
			anadirProdPA.setString(1, textFieldNombre.getText());
			anadirProdPA.setInt(2, Integer.parseInt(textFieldPrecio.getText()));
			anadirProdPA.setInt(3, estatus);
			
			anadirProdPA.execute();
			conexion.commit();
			
			
			
		} catch (Exception e) {
			conexion.rollback();
			e.printStackTrace();
		} finally {
			conexion.close();
		}
	}
}
