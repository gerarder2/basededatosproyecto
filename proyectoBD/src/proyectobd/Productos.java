package proyectobd;

public class Productos {
	private String nomProd;
	private int idProd, precProducto, estatus;
	
	public Productos(int idProd, String nomProd, int precProducto, int estatus) {
		this.idProd=idProd;
		this.nomProd=nomProd;
		this.precProducto=precProducto;
		this.estatus=estatus;
	}
	
	public int getIdProd() {
		return idProd;
	}
	public String getNombre() {
		return nomProd; 
	}
	
	public int getPrecio() {
		return precProducto;
	}
	
	public int getEstatus() {
		return estatus;
	}
	
	
}
