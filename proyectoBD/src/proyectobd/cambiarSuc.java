package proyectobd;

import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class cambiarSuc {

	public JFrame frame;
	private JTable tablaSucursales;
	private JLabel lblNewLabel;
	private JButton btnSeleccion;
	
	
	// INICIA M�TODO PARA MOSTRAR SUCURSALES
	public ArrayList<Sucursales> listaSucursales(){
		ArrayList<Sucursales> listaSuc = new ArrayList<>(); 
		Connection conexion = null;
		try {
			conexion = conectar.getConnection();
			conexion.setAutoCommit(false);
			String query="select * from Sucursales";
			Statement cst = conexion.createStatement();
			ResultSet rs = cst.executeQuery(query);
			Sucursales sucursal;
			while(rs.next()) {
				sucursal = new Sucursales(rs.getInt("idSuc"), rs.getString("nomSuc"), rs.getString("domSuc"), rs.getInt("estatus"));
				listaSuc.add(sucursal);
			}
			
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(null, e1);
		}
		return listaSuc;
	}
	
	public void mostrarProductos() {
		ArrayList<Sucursales> list = listaSucursales();
		DefaultTableModel model = (DefaultTableModel)tablaSucursales.getModel();
		Object[] fila = new Object[4];
		for(int i=0;i<list.size();i++) {
			fila[0]=list.get(i).getIdSuc();
			fila[1]=list.get(i).getNombre();
			fila[2]=list.get(i).getDom();
			fila[3]=list.get(i).getEstatus();
			model.addRow(fila);
		}
	}
	
	// TERMINA M�TODO PARA MOSTRAR SUCURSALES
	
	// INICIA M�TODO PARA SELECCIONAR SUCURSAL
	public void elegir() {
		//String nvaSuc = textFieldSeleccion.getText();
	}
	//
	
	anadirVenta mostrarCambios = new anadirVenta();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					cambiarSuc window = new cambiarSuc();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public cambiarSuc() {
		initialize();
		mostrarProductos();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	public void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 270);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(39, 54, 356, 112);
		frame.getContentPane().add(scrollPane);
		
		tablaSucursales = new JTable();
		tablaSucursales.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"ID Sucursal", "Nombre", "Domicilio", "Estatus"
			}
		) {
			boolean[] columnEditables = new boolean[] {
				false, true, true, true
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		tablaSucursales.getColumnModel().getColumn(0).setResizable(false);
		scrollPane.setViewportView(tablaSucursales);
		
		lblNewLabel = new JLabel("Seleccione la sucursal:");
		lblNewLabel.setFont(new Font("Dubai", Font.PLAIN, 24));
		lblNewLabel.setBounds(35, 11, 346, 32);
		frame.getContentPane().add(lblNewLabel);
		
		btnSeleccion = new JButton("Seleccionar");
		btnSeleccion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = tablaSucursales.getSelectedRow();
				TableModel model = tablaSucursales.getModel();
				String nombre = model.getValueAt(index, 1).toString();
				//textFieldSeleccion.setText(nombre);
				String ID = model.getValueAt(index, 0).toString();
				
				anadirVenta.textFieldSucursal.setText(ID);
				frame.setVisible(false);
				anadirVenta.frame.setVisible(true);
				
				//mostrarCambios.textFieldSucursal.setText(nombre);
				
				//frame.dispose();

			}
		});
		btnSeleccion.setBounds(271, 177, 110, 23);
		frame.getContentPane().add(btnSeleccion);
		
	}

}
