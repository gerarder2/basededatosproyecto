package proyectobd;

public class Sucursales {
	private String nomSuc, domSuc;
	private int idSuc, estatus;
	
	public Sucursales(int idSuc, String nomSuc, String domSuc, int estatus) {
		this.idSuc=idSuc;
		this.nomSuc=nomSuc;
		this.domSuc=domSuc;
		this.estatus=estatus;
	}
	
	public int getIdSuc() {
		return idSuc;
	}
	public String getNombre() {
		return nomSuc; 
	}
	
	public String getDom() {
		return domSuc;
	}
	
	public int getEstatus() {
		return estatus;
	}

}
